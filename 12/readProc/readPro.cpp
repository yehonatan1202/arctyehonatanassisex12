#include <Windows.h>
#include <iostream>

int main()
{

	HANDLE hFile = CreateFileA("gibrish.bin",
		GENERIC_ALL,
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL);
	if (hFile == INVALID_HANDLE_VALUE) //ceack if work
	{
		std::cerr << "cant open file";
		return 1;
	}
	HANDLE hMapFile = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 0, 0, "myFile");
	if (!hMapFile)
	{
		std::cerr << "fail map the file";
		return 1;
	}
	STARTUPINFO sI = { 0 };
	PROCESS_INFORMATION pI = { 0 };
	CreateProcessA(NULL, (LPSTR)"writeProc myFile", NULL, NULL, false, 0, NULL, NULL, &sI, &pI);
	WaitForSingleObject(pI.hProcess, INFINITE); //close all
	CloseHandle(pI.hThread);
	CloseHandle(hMapFile);
	return 0;
}