#include <Windows.h>
#include <iostream>
int countA(char* arr, int size);
int main()
{
	HANDLE hFile = CreateFileA("gibrish.bin",            
		GENERIC_READ,          
		0,      
		NULL,                  
		OPEN_EXISTING,         
		FILE_ATTRIBUTE_NORMAL,
		NULL);      
	if (hFile == INVALID_HANDLE_VALUE)
	{
		std::cerr << "cant open file";
		return 1;
	}
	DWORD size = GetFileSize(hFile, NULL);
	char* arr = new char[size];
	bool check = ReadFile(hFile, arr, size, NULL, NULL);
	if (!check)
	{
		std::cerr << "fail  to read file";
		return 1;
	}
	std::cout << "A times = " << countA(arr, size);
	system("pause");

	delete[] arr;
	CloseHandle(hFile);
	return 0;
}

/*
input arr, size
out put count
the function will count how many 'A' there is*/
int countA(char* arr, int size)
{
	int count = 0;
	for (int i = 0; i < size; i++)
		if (arr[i] == 'A')
			count++;
	return count;
}