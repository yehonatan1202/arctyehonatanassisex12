#include <Windows.h>
#include <iostream>


int main(int argc, char* argv[])
{
	HANDLE hMapFile = OpenFileMappingA(FILE_MAP_WRITE, true,(LPCSTR)argv[1]);
	if (hMapFile == NULL)
	{
		std::cerr << "falid open maped file";
		return 1;
	}
	char* pBuf = (LPTSTR)MapViewOfFile(
		hMapFile, 
		FILE_MAP_WRITE,  
		0,
		0,
		0);
	if (pBuf == NULL)
	{
		std::cerr << "eror with map view of file";
		return 1;
	}
	pBuf[0] = 'Z';
	UnmapViewOfFile(pBuf);
	CloseHandle(hMapFile);
	return 0;
}